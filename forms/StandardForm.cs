﻿using Verascape.Configuration;
using Verascape.VoiceXml;
using Verascape.VoiceXml.Templates;

namespace $ClientName$.Speech.VoiceXml.$ApplicationName$.$ModuleName$.Forms
{
    /// <summary>
    /// $FormNotes$
    /// </summary>
    public class $FormName$ : Form
    {
        public $FormName$(IHostConfig config, TemplateEngine te)
        {
            this.Id = "$FormID$";
            $FormCode$
        }
    }
}
