﻿using Verascape.Configuration;
using Verascape.VoiceXml;
using Verascape.VoiceXml.Templates.Forms;

namespace $ClientName$.Speech.VoiceXml.$ApplicationName$.$ModuleName$.Forms
{
    /// <summary>
    ///     EntryPoint
    /// </summary>
    [EntryPoint]
    public class EntryPoint : Form
    {
        public EntryPoint(IHostConfig config)
        {
            this.Id = "entryPoint";

            /* @FormCode.Start */
            $FormCode$
            /* @FormCode.End */
        }
    }
}