﻿using System;
using Microsoft.AspNetCore.Mvc;
using Verascape.Logging;
using Verascape.VoiceXml;
using Verascape.VoiceXml.Mvc;
using Verascape.VoiceXml.Templates;

namespace $ClientName$.Speech.VoiceXml.$ApplicationName$.$ModuleName$
{
    public class $ModuleName$Controller : VxmlController
    {
        public class $ModuleName$QueryString
        {
         
            /* @QueryFields.Start */
            
            /* @QueryFields.End */
        }

        [HttpGet("$ApplicationName_L$/$ModuleName_L$")]
        public Vxml Index([FromQuery] $ModuleName$QueryString queryString)
        {
            Templates.ChangeAudioPath(Config.Prompts.Append("$AudioPath$"));

            // This will log using the ILogManager configured in Startup.cs.  Currently set to log System Events via
            // the Api Logger.  The .Debug() function will only log it the Logmanager is setup with Debug = true.  This
            // is don't in Startup.cs if the enviroment is == Development.
            LogManager.GetLogger().Debug("[ModuleEntry]: $ClientName$.$ApplicationName$.$ModuleName$");
            
            return new Vxml()
                // Setup the Vxml element using the standard configurartion.  Adds an app (root) document reference if
                // specified.
                .StandardConfiguration(queryString.App)
                
                // Add standard .js file references
                .AddStandardScripts(Config)

                // Relevant querystring: Dnis, Ani.  Do not add the app.
                .AddVars(queryString, exclude: new[] {"app"})

                // This will add forms from the "Template.Speech.VoiceXml.MyFirstApp.Main.Forms" namespace.  The
                // IHostConfig and TemplateEngine are automatically injected into forms if required.  Additional
                // dependencies can be injected by adding them to the params [].
                .Add(Templates.FormBuilder.GetFormsFromNamespace(GetType().Assembly,
                    "$ClientName$.Speech.VoiceXml.$ApplicationName$.$ModuleName$.Forms")
                )

                // This will add forms from the "Template.Speech.VoiceXml.MyFirstApp.Common.Forms" namespace.
                .Add(Templates.FormBuilder.GetFormsFromNamespace(GetType().Assembly,
                    "$ClientName$.Speech.VoiceXml.Common.Forms"))
                .Convert(Platform.Converter);
        }
    }
}